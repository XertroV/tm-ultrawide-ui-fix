# Trackmania Ultrawide UI Fix plugin for Openplanet

This [Openplanet](https://openplanet.dev/) plugin fixes the in-game UI of Trackmania 2020 for aspect ratios wider than 16:9 by moving the UI elements back to the edge of the screen.

## Supported UI elements
* Best & previous times (`Race_BestRaceViewer`)
* Time Attack countdown (`Race_Countdown`)
* Messages in the top left (`Race_DisplayMessage`)
* Lap counter (`Race_LapsCounter`)
* Records (`Race_Record`)
* Respawn & Give up prompts (`Race_RespawnHelper`)
* Checkpoint times (`Race_TimeGap`)
* Warmup information (`Race_WarmUp`)
* COTD knockout results (`Knockout_KnockoutInfo`)
* COTD qualification countdown (`TimeAttackDaily_NextMatchTracker`)
* COTD qualification results (`TimeAttackDaily_DailyTrackerTA`)
* Royal ??? (`Royal_FinishFeed`)
* Royal team ranking (`Royal_LiveRanking`)
* Royal Restart prompt (`Royal_RespawnHelper`)
* Royal team results (`Royal_TeamScore`)
* ~~Slide out medal banner (`clip-medal-banner`)~~ - temporarily disabled because of [issue #2](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues/2)

## Not yet supported UI elements
* Small chat window ([issue #1](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues/1))
* Ranked UI (not tested, might work)

## Known bugs
* The initial position of `clip-medal-banner` is incorrect, all subsequent animations work correctly ([issue #5](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues/5))
* The medal gain animation ends at the original position, not at the side of the screen ([issue #4](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues/4))
* Alt-tabbing when the map is loading sometimes causes the UI elements to not move ([issue #6](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues/6))
* Changing resolutions when the game is running has no effect on the UI elements, the original resolution is used ([issue #3](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues/3))

# How to use this plugin
* Trackmania 2020 with Openplanet installed is required
* Search for `Ultrawide UI Fix` in the Plugin Manager
* Alternatively, download the plugin from its [Openplanet plugin page](https://openplanet.dev/plugin/ultrawideuifix) and place it Openplanet's Plugins directory

## Source code
The source code of this plugin is available on [GitLab](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix) under the MIT license.

## Bugs & issues
If you run into any bugs or issues, report them in the [issue tracker](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues).

## Authors
* \>= 3.0.0 - Dan_VQ / Daniel Peukert
* < 3.0.0 - [Eierpflanze / Felix Meyer](https://github.com/Eierpflanze)
