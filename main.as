[Setting name="Print debug log statements"]
bool Setting_Debug = false;

namespace UltrawideUiFix {
	const string MODIFIED_DATA_ATTRIBUTE = 'ultrawideuifix-modified';

	const string MODULE_CLASS = 'component-modelibs-uimodules-module';

	const string MODULE_PREFIX = 'UIModule_';

	const string MANIALINK_TAG = '\n<manialink name="';

	const string CONTENT_FRAME_ID = 'frame-content';

	const string[] MODULES_TO_MOVE = {
		'Race_BestRaceViewer',
		'Race_Countdown',
		'Race_DisplayMessage',
		'Race_LapsCounter',
		'Race_Record',
		'Race_RespawnHelper',
		'Race_TimeGap',
		'Race_WarmUp',
		'Knockout_KnockoutInfo',
		'TimeAttackDaily_NextMatchTracker',
		'TimeAttackDaily_DailyTrackerTA',
		'Royal_FinishFeed',
		'Royal_LiveRanking',
		'Royal_RespawnHelper',
		'Royal_TeamScore'
	};

	dictionary CLIPS_TO_EXPAND = {
		{
			'Race_Record', array<string> = {
				'clip-medal-banner'
			}
		},
		{
			'TimeAttackDaily_DailyTrackerTA', array<string> = {
				'frame-global'
			}
		},
		{
			'TimeAttackDaily_NextMatchTracker', array<string> = {
				'frame-global'
			}
		}
	};

	dictionary ANIMATIONS_TO_MODIFY = {
		{
			'Race_Record', array<dictionary> = {
				{
					{'regex', """AnimMgr\.Add\(Controls\.Frame_Medal, "<a pos=\\"([^ ]+) ([^ ]+)\\"""},
					{'replace', 'AnimMgr.Add(Controls.Frame_Medal, "<a %MOD%pos=\\"%X% %Y%\\'}
				}
			}
		}
	};

	namespace Fixes {
		void fixAnimations(const string &in moduleId, float uiShift, CGameUILayer@ layer) {
			// Check if we have any animations to modify for this module
			if (ANIMATIONS_TO_MODIFY.Exists(moduleId)) {
				dictionary[] animations = cast<array<dictionary>>(ANIMATIONS_TO_MODIFY[moduleId]);

				for (uint animationIndex = 0; animationIndex < animations.Length; ++animationIndex) {
					// Look for matches for every animation regex for the given module
					Regex::SearchAllResult@ animationRegex = Regex::SearchAll(layer.ManialinkPageUtf8, string(animations[animationIndex]['regex']));

					for (uint animationRegexMatchIndex = 0; animationRegexMatchIndex < animationRegex.Length; ++animationRegexMatchIndex) {
						string[] animationRegexMatch = animationRegex[animationRegexMatchIndex];

						// Check if we have both coordinate capture groups
						if (animationRegexMatch.Length == 3) {
							// Prepare our coordinates
							float oldX = Text::ParseFloat(animationRegexMatch[1]);
							float animationShift = (oldX >= 0 ? 1 : -1) * uiShift;
							string newX = Text::Format('%.0f', oldX + animationShift);

							// Prepare our replace strings
							string search = string(animations[animationIndex]['replace']).Replace('%MOD%', '').Replace('%X%', animationRegexMatch[1]).Replace('%Y%', animationRegexMatch[2]);
							string replace = string(animations[animationIndex]['replace']).Replace('%MOD%', 'data-' + MODIFIED_DATA_ATTRIBUTE + '=\\"1\\" ').Replace('%X%', newX).Replace('%Y%', animationRegexMatch[2]);

							// Replace the animations in the Manialink code
							layer.ManialinkPageUtf8 = layer.ManialinkPageUtf8.Replace(search, replace);
							print('Coordinates of an animation in ' + moduleId + ' moved by ' + animationShift + ' units');

							if (Setting_Debug) {
								trace('Animation string "' + search + '" has been replaced with "' + replace + '"');
							}
						}
					}
				}
			}
		}



		void fixModule(const string &in moduleId, float uiShift, CGameManialinkControl@ control) {
			// Check if we want to modify this module
			if (MODULES_TO_MOVE.Find(moduleId) >= 0) {
				// Check if the module was already moved
				if (control.DataAttributeGet(MODIFIED_DATA_ATTRIBUTE) != tostring(uiShift)) {
					float prevShift = 0.0;
					if (control.DataAttributeGet(MODIFIED_DATA_ATTRIBUTE).Length > 0) {
						prevShift = Text::ParseFloat(control.DataAttributeGet(MODIFIED_DATA_ATTRIBUTE));
					}
					// The module wasn't moved, calculate the shift and move it
					float controlShift = (control.PosnX >= 0 ? 1 : -1) * (uiShift - prevShift);
					control.PosnX += controlShift;
					print('Module ' + moduleId + ' moved by ' + controlShift + ' units');

					if (Setting_Debug) {
						trace('Module ' + moduleId + ' has been moved from X=' + (control.PosnX - controlShift) + ' to X=' + control.PosnX);
					}

					// Mark the module as modified
					control.DataAttributeSet(MODIFIED_DATA_ATTRIBUTE, tostring(uiShift));
				} else if (Setting_Debug) {
					trace('Module ' + moduleId + ' already moved, ignoring');
				}
			}
		}

		void fixClips(const string &in moduleId, float uiShift, CGameManialinkPage@ page) {
			// Check if we have any clips to modify for this module
			if (CLIPS_TO_EXPAND.Exists(moduleId)) {
				string[] clips = cast<array<string>>(CLIPS_TO_EXPAND[moduleId]);

				for (uint clipIndex = 0; clipIndex < clips.Length; ++clipIndex) {
					// Check if a control exists for every clip ID provided for the given module
					CGameManialinkControl@ clip = page.GetFirstChild(clips[clipIndex]);

					if (clip is null) {
						continue;
					}

					string clipId = clip.ControlId;

					// Check if the module was already expanded
					if (clip.DataAttributeGet(MODIFIED_DATA_ATTRIBUTE) == '') {
						// The module wasn't expanded, calculate the expansion and expand it
						float clipExpand = 2 * uiShift;
						clip.Size = vec2(clip.Size.x + clipExpand, clip.Size.y);
						print('Clip ' + clipId + ' expanded by ' + clipExpand + ' units');

						if (Setting_Debug) {
							trace('Clip ' + clipId + ' has been expanded from W=' + (clip.Size.x - clipExpand) + ' to W=' + clip.Size.x);
						}

						// Mark the clip as modified
						clip.DataAttributeSet(MODIFIED_DATA_ATTRIBUTE, '1');
					} else if (Setting_Debug) {
						trace('Clip ' + clipId + ' already expanded, ignoring');
					}
				}
			}
		}
	}

	class Fixer {
		// Whether our fixes should be applied during the next frame, used to make sure layer data is available
		bool applyNextFrame = false;

		// The amount of layers that existed in the previous cycle, used to make sure we only apply fixes when the UI changes
		uint previousLayerLength = 0;

		// Get the amount of units to shift UI elements by (https://maniaplanet-community.gitbook.io/maniascript/manialink/manialinks#manialink-positioning-system)
		// float uiShift = Math::Max(0, ((float(Draw::GetWidth()) / (float(Draw::GetHeight()) / 9)) * 10) - 160);

		// Get the amount of units to shift UI elements by (https://maniaplanet-community.gitbook.io/maniascript/manialink/manialinks#manialink-positioning-system)
		float uiShift {
			get {
				float h = Math::Max(1.0, Draw::GetHeight());
				return Math::Max(0.0, (float(Draw::GetWidth()) / (h / 9)) * 10 - 160);
			}
		}

		// Normalize UI layer buffer
		MwFastBuffer<CGameUILayer@> layers {
			get const {
				CGameManiaAppPlayground@ playground = GetApp().Network.ClientManiaAppPlayground;

				// Check if layers are available
				if (playground !is null) {
					return playground.UILayers;
				} else {
					// Layers are not yet available, use an empty buffer
					MwFastBuffer<CGameUILayer@> no_layers;
					return no_layers;
				}
			}
		};

		// Check if the game is using a higher aspect ratio than 16:9
		bool IsUiWide() {
			return uiShift > 0;
		}

		// Check if the amount of UI layers changed since the last call
		bool HasUiChanged() {
			return layers.Length != previousLayerLength;
		}

		// Apply our position and size fixes
		void ApplyFixes() {
			trace("Applying Ultrawide Fixes");
			// Loop through each layer
			for (uint layerIndex = 0; layerIndex < layers.Length; ++layerIndex) {
				CGameUILayer@ layer = layers[layerIndex];
				CGameManialinkPage@ page = layer.LocalPage;

				CGameManialinkControl@ control = null;
				string moduleId;

				// Check if we have the main UI module
				page.GetClassChildren(MODULE_CLASS, page.MainFrame, true);

				if (page.GetClassChildren_Result.Length > 0) {
					@control = page.GetClassChildren_Result[0];
					moduleId = control.ControlId;
				} else {
					// If not, check if we have a manialink tag with a name attribute
					string manialinkSnippet = layer.ManialinkPageUtf8.SubStr(0, 100);

					if (manialinkSnippet.StartsWith(MANIALINK_TAG)) {
						// If we do have one, get the value of the attribute and check if it has the UI module prefix
						string[] manialinkSnippetParts = manialinkSnippet.Split('"');

						if (manialinkSnippetParts.Length > 1) {
							string manialinkName = manialinkSnippetParts[1];

							if (manialinkName.StartsWith(MODULE_PREFIX)) {
								// If it does strip it and keep the rest
								moduleId = manialinkName.SubStr(MODULE_PREFIX.Length);

								// Check if we have a frame-content frame, if we do, use it as the control
								@control = page.GetFirstChild(CONTENT_FRAME_ID);

								if (control is null) {
									if (Setting_Debug) {
										trace('Unable to find content frame in layer ' + moduleId);
									}
									continue;
								}
							}
						}
					}
				}

				if (moduleId == '' || control is null) {
					if (Setting_Debug) {
						trace('Unable to identify layer ' + layer.IdName);
					}
					continue;
				}

				// Modify animations in Manialink code
				// Temporarily disabled, as Regex::SearchAll sometimes causes crashes (see https://github.com/openplanet-nl/issues/issues/198)
				//Fixes::fixAnimations(moduleId, uiShift, @layer);

				// Move the main UI module using properties
				Fixes::fixModule(moduleId, uiShift, @control);

				// Resize clips using properties
				Fixes::fixClips(moduleId, uiShift, @page);
			}

			// TODO: move small chat window

			// Save the layer count
			previousLayerLength = layers.Length;
		}
	}
}

// The main instance of our logic class
UltrawideUiFix::Fixer@ fixer = null;

// Main entry point
void Main() {
	// Instantiate our logic class
	UltrawideUiFix::Fixer mainFixer;

	// Store the instance as the main one
	@fixer = @mainFixer;
	// Check if we are working with a wide aspect ratio, no point in checking for UI changes otherwise
	// TODO: handle resolution changes during gameplay
	// if (mainFixer.IsUiWide()) {
	// } else {
	// 	print('Resolution is not wider than 16:9, no further checks will be done');
	// }
}

string lastMapUid;
bool wasUltrawide = false;
float lastUiShift = 0.;
void Render() {
	// Check if we should be checking for UI changes
	if (fixer !is null) {
		bool isUltrawide = fixer.IsUiWide();

		auto app = GetApp();
		if (app.CurrentPlayground is null) {
			fixer.previousLayerLength = 0;
			lastMapUid = "";
		} else if (app.RootMap !is null) {
			if (lastMapUid != app.RootMap.MapInfo.MapUid) {
				lastMapUid = app.RootMap.MapInfo.MapUid;
				fixer.previousLayerLength = 0;
			}
		} else {
			lastMapUid = "";
		}

		// First, check if we should apply fixes based on the previous frame
		if (fixer.applyNextFrame == true) {
			fixer.ApplyFixes();
			fixer.applyNextFrame = false;
		}

		// Check if there were any UI changes, if so, schedule applying our fixes for the next frame
		bool changed = fixer.HasUiChanged() || wasUltrawide != isUltrawide
			|| lastUiShift != fixer.uiShift;
		if (changed) {
			fixer.applyNextFrame = true;
		}
		wasUltrawide = isUltrawide;
		lastUiShift = fixer.uiShift;
	}
}
